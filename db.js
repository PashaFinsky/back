const dbreq = require('./dbreq')

exports.getTable = (table) =>
{
    return new Promise((resolve) => {
        resolve(dbreq.dbRequest("SELECT * FROM " + table))
    })
}

exports.insertIntoTable = (table, values) =>
{
    return new Promise(resolve => {
        resolve(dbreq.dbRequest("INSERT INTO " + table + " (name, surname) VALUES ?", values))
    })
}