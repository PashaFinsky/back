exports.dbRequest = (sql, values) =>
{
    return new Promise((resolve, reject) =>
    {
        connect.getConnection((err, connection)=>
        {
            if (err)
            {
                console.log("err 1")
                throw err
            }
            connection.query(sql, [[values]], (err, result) =>
            {
                if(err)
                {
                    console.log("err 2")
                    reject(err)
                }
                console.log(result)
                resolve(result)
            })
        })
    })
}