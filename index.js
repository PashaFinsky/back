const express = require('express')
const bodyParser = require('body-parser')
global.app = express()

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

const routes = require('./routes')


const mysql = require('mysql2')

const con = mysql.createPool({
    host: 'localhost',
    user: 'root',
    database: 'mydb',
    password: '',
    connectionLimit: 100
})



global.connect = con

app.listen(3000)
console.log("it's ok")