const controller = require('./controller')

app.route('/Table')
        .get(async (req, res) => {
            res.send(await controller.getTable('users'))
        })

app.route('/UserInput')
        .post(async (req, res) =>
        {
            console.log(req.body)
            console.log('routes!!!!!!!!!!!')
            res.send(await controller.insertIntoTable('users', req.body))
        })